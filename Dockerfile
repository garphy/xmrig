FROM alpine:latest AS builder

RUN apk add git make cmake libstdc++ gcc g++ automake libtool autoconf linux-headers patch bash
RUN git clone https://github.com/xmrig/xmrig.git
ADD donate.patch donate.patch
RUN cd xmrig ; patch -p1 < ../donate.patch
RUN mkdir xmrig/build
RUN cd xmrig/scripts && ./build_deps.sh && cd ../build
RUN cd xmrig/build && cmake .. -DXMRIG_DEPS=scripts/deps -DBUILD_STATIC=ON
RUN cd xmrig/build && make -j$(nproc)

FROM alpine:latest

COPY --from=builder /xmrig/build/xmrig /usr/local/bin/xmrig

CMD /usr/local/bin/xmrig

